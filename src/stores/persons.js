import { defineStore } from 'pinia'
import axios from 'axios'

export const usePersonStore = defineStore('person', {
  state: () => ({
    persons: [{firstName: 'Max', lastName: 'Muster'}]
  }),
  actions: {
    async loadAllUsers() {
      const response = await axios.get('https://crudcrud.com/api/adb14271d1cf4c408193fbec1a113db3/users')
      this.persons = response.data
    },
    async createUser(firstName, lastName) {
      const response = await axios.post('https://crudcrud.com/api/adb14271d1cf4c408193fbec1a113db3/users', {
        firstName,
        lastName
      })
      console.log(response)
      this.persons.push(response.data)
    },
    async deleteUser(id) {
      const response = await axios.delete('https://crudcrud.com/api/adb14271d1cf4c408193fbec1a113db3/users/' + id)
      console.log(response)
      const personIndex = this.persons.findIndex(person => person._id === id)
      if(personIndex >= 0) {
        this.persons.splice(personIndex, 1)
      }
    }
  }
})
